use rand::Rng;

use std::time::Instant;

// monitoring statistics for bootraping
#[derive(Debug)]
struct Cycle {
    // cycle id
    id: usize,
    // number of downloaded block headers per cycle
    headers: usize,
    // number of downloaded block operatios per cycle
    operations: usize,
    // number of applied blocks
    applications: usize,
    // timestamp for fisrt block header occurrence
    start: Instant,
    // time to download headers and operations for cycle
    duration: Option<f32>,
}

fn main() {
    // cycle stats for chain
    let mut chain: Vec<Cycle> = vec![];

    // number of block
    const NUMBER_OF_BLOCKS: usize = 1000000;

    // number of block
    const BLOCKS_PER_CYCLE: usize = 4096;

    // process new block header
    fn received_block_header(chain: &mut Vec<Cycle>, block_level: usize) -> &mut Vec<Cycle> {
        let cycle_id = block_level / BLOCKS_PER_CYCLE;
        let chain_len = chain.len();
        // println!("[block] header level: {:.8} cycle_id: {} chain_len: {}",  block_level, cycle_id, chain_len);

        // create all missing cycles for chain stats
        if cycle_id >= chain_len {
            let mut chain_append = (0..(cycle_id - chain_len + 1))
                .map(|id| -> Cycle {
                    Cycle {
                        id: chain_len + id,
                        headers: 0,
                        operations: 0,
                        applications: 0,
                        start: Instant::now(),
                        duration: None,
                    }
                })
                .collect();
            // append created chain stats
            chain.append(&mut chain_append);
        };

        // use reference to cycle stats
        let cycle = &mut chain[cycle_id];
        // increment block headers value
        cycle.headers += 1;
        // save duration for cycle download time at end of cycle
        if cycle.headers == BLOCKS_PER_CYCLE {
            // save duration in seconds
            cycle.duration = Some(cycle.start.elapsed().as_secs_f32())
        }

        // return chain
        chain
    };

    // process new block operations
    // let received_block_operations = |block_level| {
    //     println!("[block] operation level: {}", block_level );
    // };
    // start random number generator
    let mut rng = rand::thread_rng();

    // create 1000 block
    for _index in 0..2500000 {
        // generate block level
        let block_level: usize = rng.gen_range(0, NUMBER_OF_BLOCKS);
        // process new block header
        received_block_header(&mut chain, block_level);

        // process new block operations
        // received_block_operations(block_level);
    }

    println!(
        "\n{:#?}  [chain] size: {} capacity: {} ",
        chain,
        chain.len(),
        chain.capacity()
    );
}
