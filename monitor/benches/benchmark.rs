use criterion::*;

// bench two vec concat
// https://stackoverflow.com/questions/40792801/best-way-to-concatenate-vectors-in-rust

fn consumed() {
    let a: Vec<i32> = (1..100000).map(|x| x).collect();
    let b: Vec<i32> = (1..100000).map(|x| x).collect();

    let _c: Vec<i32> = a.into_iter().chain(b.into_iter()).collect(); // Consumed
}

fn referenced() {
    let a: Vec<i32> = (1..100000).map(|x| x).collect();
    let b: Vec<i32> = (1..100000).map(|x| x).collect();

    let _c: Vec<&i32> = a.iter().chain(b.iter()).collect(); // Referenced
}

fn cloned() {
    let a: Vec<i32> = (1..100000).map(|x| x).collect();
    let b: Vec<i32> = (1..100000).map(|x| x).collect();

    let _c: Vec<i32> = a.iter().cloned().chain(b.iter().cloned()).collect(); // Cloned
}

fn copied() {
    let a: Vec<i32> = (1..100000).map(|x| x).collect();
    let b: Vec<i32> = (1..100000).map(|x| x).collect();

    let _c: Vec<i32> = a.iter().copied().chain(b.iter().copied()).collect(); // Copied
}

fn appended() {
    let mut a: Vec<i32> = (1..100000).map(|x| x).collect();
    let mut b: Vec<i32> = (1..100000).map(|x| x).collect();

    a.append(&mut b);

    // a.extend(b.into_iter())

}

fn bench(c: &mut Criterion) {
    // c.bench_function("cloned", move |b| b.iter(|| cloned()));
    // c.bench_function("copied", move |b| b.iter(|| copied()));
    // c.bench_function("referenced", move |b| b.iter(|| referenced()));
    // c.bench_function("consumed", move |b| b.iter(|| consumed()));
    c.bench_function("appended", move |b| b.iter(|| appended()));
}

criterion_group!(benches, bench);
criterion_main!(benches);
